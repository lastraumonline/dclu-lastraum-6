const canvas = new UICanvas()
const message = new UIText(canvas)
message.value = '*Tip - find the entity to click that resets the scene *'
message.positionX = 30
message.positionY = 30
message.hAlign = 'left'
message.vAlign = 'bottom'
message.width = 250
message.color = Color4.Black()
message.fontSize = 20
message.textWrapping = true

export class NewScene extends Entity
{
  constructor()
  {
    super("lastraum-dclu")
    this.addComponentOrReplace(new Transform({
      position: new Vector3(0, 0, 0),
      rotation: Quaternion.Euler(0, 0, 0),
      scale: new Vector3(1, 1, 1)
    }))

    const base = new Entity()
    base.addComponentOrReplace(new PlaneShape())
    base.addComponentOrReplace(new Transform({
      position: new Vector3(8, 0, 8),
      rotation:Quaternion.Euler(90,0,90),
      scale: new Vector3(16,16,16)
    }))
    base.addComponentOrReplace(this.getMaterial())
    base.addComponentOrReplace(new OnClick(e=>{
      box.addComponentOrReplace(this.getMaterial())
    }))
    base.setParent(this)
    
    const box = new Entity()
    box.addComponent(new BoxShape())
    box.addComponentOrReplace(new Transform({
    position:new Vector3(4,3,4),
    scale:Vector3.One()
    }))
    box.addComponentOrReplace(this.getMaterial())
    box.addComponentOrReplace(new OnClick(e=>{
      box2.addComponentOrReplace(this.getMaterial())
    }))
    box.setParent(this)
    
    const cone = new Entity()
    cone.addComponent(new ConeShape())
    cone.addComponentOrReplace(new Transform({
    position:new Vector3(6,1,10),
    scale:Vector3.One()
    }))
    cone.addComponentOrReplace(this.getMaterial())
    cone.addComponentOrReplace(new OnClick(e=>{
      base.addComponentOrReplace(this.getMaterial())
    }))
    cone.setParent(this)
    
    const box2 = new Entity()
    box2.addComponent(new BoxShape())
    box2.addComponentOrReplace(new Transform({
    position:new Vector3(3,1,5),
    scale:Vector3.One()
    }))
    box2.addComponentOrReplace(this.getMaterial())
    box2.addComponentOrReplace(new OnClick(e=>{
      cone.addComponentOrReplace(this.getMaterial())
    }))
    box2.setParent(this)
    
    
    var hasClicked = false
    for(var i = 0; i < 20; i++)
    {
      var ent = new Entity()
      var shape = Math.floor(Math.random() * 3)
      switch(shape)
      {
        case 1:
          ent.addComponentOrReplace(new BoxShape())
          break;
        case 2:
          ent.addComponentOrReplace(new ConeShape())
          break;
        case 3:
          ent.addComponentOrReplace(new SphereShape())
          break;
      }
      ent.addComponentOrReplace(new Transform({
        position: new Vector3(Math.floor(Math.random()*16),Math.floor(Math.random()*(6-3+1)+3),Math.floor(Math.random()*16)),
        scale: Vector3.One()
      }))
      ent.addComponentOrReplace(this.getMaterial())
      var clickable = Math.floor(Math.random() * 2)
      if(clickable == 1 && !hasClicked)
      {
        ent.addComponentOrReplace(new OnClick(e=>{

          const removeE = engine.getComponentGroup(Transform)
          for(let rem of removeE.entities)
          {
            engine.removeEntity(rem)
          }
          engine.addEntity(new NewScene())
        }))
        hasClicked = true
      }
      ent.setParent(this)
    }

  }

  getMaterial()
  {
    var material = new Material()
    material.albedoColor = Color3.Random()
    material.emissiveColor = Color3.Random()
    material.metallic = Math.floor(Math.random() *1)
    material.roughness = Math.floor(Math.random() *1)
    material.emissiveIntensity = Math.floor(Math.random() *1)
    return material
  }
}


engine.addEntity(new NewScene())